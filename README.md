# Curso de React

Siguiendo un curso de react en Udemy :·)

## Requisitos para este repositorio

Para no manchar el entorno local de mi máquina voy a usar una máquina virtual
en VirtualBox y voy a usar Vagrant para hacer el aprovisionamiento y gestionar
el ciclo de vida de la máquina.

## Instalando las dependencias

### Instalando Nodejs, NPM y NPX

Para instalar node hay que bajar el binario apropiado o los sources, en nuestro
caso bajamos los binarios desde la [página oficial de descargas de
nodejs](https://nodejs.org/es/download/).

Y luego seguimos las instrucciones de instalación de la [wiki oficial de
nodejs](https://github.com/nodejs/help/wiki/Installation#how-to-install-nodejs-via-binary-archive-on-linux)

1. Descargar node, que incluye npm y npx.
1. Instalar node siguiendo las instrucciones.

### Herramienta para crear aplicaciones React

Lo primero que vamos a tener que hacer para crear una aplicación de React es
descargar la herramienta que nos crea el esqueleto de la aplicación (ver nota
si da problemas).

    npm install -g create-react-app

después deberemos crear una aplicación de prueba:

    create-react-app udemy-curso

Lo que se baja medio internet, así que paciencia.

NOTA: Probablemente falle al instalar "create-react-app", en ese caso hay que
aplicar los permisos correctos en el directorio donde se van a instalar los
paquetes globales. Con el siguiente mandato se soluciona:

    sudo chown -R vagrant:vagrant /usr/local/lib/nodejs
